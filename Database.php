<?php

namespace FpDbTest;

use Exception;

class Database implements DatabaseInterface
{
    public function buildQuery(string $template, array $params = []): string
    {
        $pattern = '/\?([dfas#]?)/';

        $replaceCallback = function ($matches) use (&$params) {
            $param = array_shift($params);
            $type = $matches[1] ?? '';

            if ($param === null) {
                return 'NULL';
            }
            if ($type === 'd' && $param === $this->skip()) {
                return '';
            }
            switch ($type) {
                case 'd':
                    return intval($param);
                case 'f':
                    return floatval($param);
                case 'a':
                    if (!is_array($param)) {
                        throw new Exception('Parameter for ?a must be an array.');
                    }
                    if (empty($param)) {
                        return 'NULL';
                    }
                    if (array_keys($param) === range(0, count($param) - 1)) {
                        return implode(', ', array_map(fn($val) => $this->escape($val), $param));
                    } else {
                        return implode(', ', array_map(fn($key, $val) => "`$key` = " . $this->escape($val), array_keys($param), $param));
                    }
                case '#':
                    if (!is_array($param)) {
                        $param = [$param];
                    }
                    return implode(', ', array_map(fn($id) => "`$id`", $param));
                case 's':
                default:
                    return $this->escape($param);
            }
        };

        $query = preg_replace_callback($pattern, $replaceCallback, $template);

        $query = preg_replace('/AND block = (?!1)(?=[^\d])/', '', $query);

        $query = str_replace(['{', '}'], '', $query);

        return $query;
    }


    public function skip()
    {
        return '__SKIP__';
    }

    private function escape(?string $value): string
    {

        if ($value === null) {
            return 'NULL';
        }
        return "'" . addslashes($value) . "'";
    }
}
